#!/usr/bin/env bash

# Get the directory of the current script
SCRIPT_DIR=$(dirname "$(realpath "$0")")

# Line to add to .bashrc
LINE="source $SCRIPT_DIR/git-short"

if [ "$SHELL" == "/usr/bin/bash" ]; then
    PROFILE_FILE=~/.bashrc
elif [ "$SHELL" == "/bin/bash" ]; then
    PROFILE_FILE=~/.bashrc
elif [ "$SHELL" == "/usr/bin/zsh" ]; then
    PROFILE_FILE=~/.zshrc
elif [ "$SHELL" == "/usr/bin/fish" ]; then
    PROFILE_FILE=~/.config/fish/config.fish
else
    echo "Your shell or distro is not compatible, add '$LINE' to your shell config manually Your shell is $SHELL"
    exit 1
fi

echo -e "Selected shell: $SHELL"
# Check if the line already exists in env
if ! grep -Fxq "$LINE" $PROFILE_FILE; then
    # Append the line to env
    echo "$LINE" >> $PROFILE_FILE
    echo -e "Added to $PROFILE_FILE: $LINE\nRestart your shell or terminal by typing\nsource $PROFILE_FILE\n"
else
    echo -e "The line is already present in $PROFILE_FILE\nRestart your shell or terminal by typing\nsource $PROFILE_FILE\n"
fi

